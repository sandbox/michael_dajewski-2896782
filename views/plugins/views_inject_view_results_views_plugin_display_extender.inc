<?php

/**
 * @file
 * Contains the views_inject_view_results views_plugin_display_extender.
 */

/**
 * The plugin that handles an 'views inject' display.
 *
 * Views Inject View Results allows to inject into view result the rows
 * from other view at fixed position.
 *
 * @ingroup views_display_plugins
 */
class ViewsInjectViewResultsViewsPluginDisplayExtender extends views_plugin_display_extender {

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_definition_alter(&$options) {
    $options['view_inject_view_results_settings'] = array(
      'default' => 0,
    );
  }

  /**
   * Provide a form to edit options for this plugin.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);

    if ($form_state['section'] === 'views_inject_view_results') {

      $view_inject_settings = is_null($form_state['input']['view_inject_name_display_id']) ? $this->display->get_option('view_inject_view_results_settings') : _views_inject_view_results_parse_settings($form_state['input']['view_inject_name_display_id']);
      $view_inject_name_display_id = $view_inject_settings['display_id'] ? $view_inject_settings['name'] . ':' . $view_inject_settings['display_id'] : NULL;

      $exclude_view = $this->view->name . ':' . $this->view->current_display;
      $options = array('' => t('-Select-'));
      $options += views_get_views_as_options(FALSE, 'enabled', $exclude_view, TRUE, TRUE);
      $form['view_inject_name_display_id'] = array(
        '#type' => 'select',
        '#title' => t('View to inject'),
        '#default_value' => $view_inject_name_display_id,
        '#description' => t("The view to inject it's results to current display."),
        '#options' => $options,
        '#ajax' => array(
          'path' => views_ui_build_form_url($form_state),
        ),
        '#submit' => array('views_inject_view_results_display_config_form_submit_temporary'),
        '#executes_submit_callback' => TRUE,
      );

      $base_tables = $this->view->get_base_tables();
      $type = 'sort';
      $field_options = views_fetch_fields(array_keys($base_tables), $type, FALSE);

      $field_info = field_info_field_map();
      $integer_type = array_filter($field_info,
        function ($var) {
          if (substr($var['type'], strlen($var['type']) - strlen('integer')) === 'integer') {
            return $var;
          }
        }
      );

      $filtered_options = array_filter($field_options,
        function ($var) use ($integer_type) {
          preg_match('#\((.*?)\)#', $var['title'], $match);
          if (array_key_exists($match[1], $integer_type)) {
            return $var;
          }
        }
      );

      $options = array();
      foreach ($filtered_options as $field => $handler) {
        $options[$field] = $handler['group'] . ': ' . $handler['title'];
      }
      $options_descriptions = array();
      foreach ($filtered_options as $field => $handler) {
        $options_descriptions[$field] = $handler['help'];
      }

      $view_inject_order_field = is_null($form_state['input']['view_inject_order_field']) ? $view_inject_settings['order_field']['table'] . '.' . $view_inject_settings['order_field']['field'] : $form_state['input']['view_inject_order_field'];

      if ($view_inject_name_display_id) {
        $form['view_inject_order_field'] = array(
          '#type' => 'radios',
          '#title' => t('Select position field'),
          '#default_value' => $view_inject_order_field,
          '#description' => t("<br>Above are available 'integer' type fields.<br><br><i>Only 'integer' type fields may be used as 'index' for the rows to be injected.</i>"),
          '#options' => $options,
          '#options_descriptions' => $options_descriptions,
          '#validated' => TRUE,
          '#after_build' => array('_views_inject_view_results_option_descriptions'),
        );
      }

      $form['#title'] .= $this->definition['title'];
    }
  }

  /**
   * Handle any special handling on the validate form.
   */
  function options_submit(&$form, &$form_state) {
    if ($form_state['section'] === 'views_inject_view_results') {
      $view_inject_settings = _views_inject_view_results_parse_settings($form_state['values']['view_inject_name_display_id']);
      if (!empty($form_state['values']['view_inject_name_display_id'])) {
        $view_inject_settings['order_field'] = array_combine(array('table', 'field'), explode('.', $form_state['values']['view_inject_order_field']));
        $this->display->set_option('view_inject_view_results_settings', $view_inject_settings);
      }
      else {
        $this->display->set_option('view_inject_view_results_settings', NULL);
      }
    }
  }

  /**
   * Provide the default summary for options in the views UI.
   *
   * This output is returned as an array.
   */
  function options_summary(&$categories, &$options) {
    $view_inject_settings = $this->display->get_option('view_inject_view_results_settings');
    $view_inject_name_display_id = !empty($view_inject_settings['display_id']) ? $view_inject_settings['name'] . ' : ' . $view_inject_settings['display_id'] : NULL;
    $options['views_inject_view_results'] = array(
      'category' => 'access',
      'title' => $this->definition['title'],
      'value' => empty($view_inject_name_display_id) ? 'No view selected' : $view_inject_name_display_id . ($view_inject_settings['order_field']['field'] ? '' : ' | No position field selected'),
      'desc' => t('Select a view to inject.'),
    );
  }

}

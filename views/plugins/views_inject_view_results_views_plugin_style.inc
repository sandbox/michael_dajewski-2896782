<?php

/**
 * @file
 * Contains the views_inject_view_results style plugin.
 */

/**
 * Style plugin to render using two display handlers.
 *
 * Override:
 *   render_grouping_sets() to render with alternate style_plugin->row_plugin
 *   render_fields() avoid rendering rows with different items ($keys) than
 *                   current handler field list.
 *
 * @ingroup views_style_plugins
 */
class ViewsInjectViewResultsViewsPluginStyle extends views_plugin_style {

  /**
   * Store all available tokens row rows.
   *
   * @var views_plugin_row
   */
  var $row_tokens = array();

  /**
   * Contains the row plugin, if it's initialized and the style does support it.
   *
   * @var views_plugin_row
   */
  var $row_plugin;

  /**
   * Render the grouping sets.
   *
   * Plugins may override this method if they wish some other way of handling
   * grouping.
   *
   * @param array $sets
   *   Array containing the grouping sets to render.
   * @param int $level
   *   Integer indicating the hierarchical level of the grouping.
   *
   * @return string
   *   Rendered output of given grouping sets.
   */
  function render_grouping_sets($sets, $level = 0) {
    $output = '';
    if ($this->view->display_handler->get_option('view_inject_view_results_settings')) {
      foreach ($sets as $set) {
        foreach ($set['rows'] as $index => $row) {
          $this->view->row_index = $index;
          if (property_exists($row, 'inject_info')) {
            $this->view->view_to_inject->row_index = $row->original_row_index;
            $set['rows'][$index] = $this->view->view_to_inject->style_plugin->row_plugin->render($row);
          }
          else {
            $set['rows'][$index] = $this->view->style_plugin->row_plugin->render($row);
          }
        }
        $output .= theme($this->theme_functions(),
          array(
            'view' => $this->view,
            'options' => $this->options,
            'grouping_level' => $level,
            'rows' => $set['rows'],
            'title' => $set['group'],
          )
        );
      }
    }
    else {
      $output = parent::render_grouping_sets($sets);
    }
    return $output;
  }

  /**
   * Render all of the fields for a given style and store them on the object.
   *
   * @param array $result
   *   The results from $view->result.
   */
  function render_fields($result) {
    if (!$this->uses_fields()) {
      return;
    }
    if ($this->view->display_handler->get_option('view_inject_view_results_settings')) {
      if (!isset($this->rendered_fields)) {
        $this->rendered_fields = array();
        $this->view->row_index = 0;
        $keys = array_keys($this->view->field);

        // If all fields have a field::access FALSE there might be no fields, so
        // there is no reason to execute this code.
        if (!empty($keys)) {
          foreach ($result as $count => $row) {

            // Do not render if the $keys do not match $row fields
            // Otherwise the views_handler_field_field->get_items() will return:
            // Notice: Undefined property: stdClass.
            // Which will cascade to:
            // Warning: Invalid argument supplied for foreach() in
            // views_handler_field->advanced_render().
            // This is because foreach() expects an array as argument.
            //
            if (!isset($row->inject_info['display_id'])) {
              $this->view->row_index = $count;
              foreach ($keys as $id) {
                $this->rendered_fields[$count][$id] = $this->view->field[$id]->theme($row);
              }

              $this->row_tokens[$count] = $this->view->field[$id]->get_render_tokens(array());
            }
          }
        }
        unset($this->view->row_index);
      }

      return $this->rendered_fields;
    }
    else {
      parent::render_fields($result);
    }
  }

}

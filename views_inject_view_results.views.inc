<?php

/**
 * @file
 * Provides plugin declarations.
 */

/**
 * Implements hook_views_plugins().
 */
function views_inject_view_results_views_plugins() {
  $path = drupal_get_path('module', 'views_inject_view_results');
  $plugins = array();
  $plugins['module'] = 'views_inject_view_results';
  $plugins['display_extender'] = array(
    'views_inject_view_results' => array(
      'title' => t('Inject view results'),
      'help' => t('A description.'),
      'path' => $path . '/views/plugins',
      'handler' => 'ViewsInjectViewResultsViewsPluginDisplayExtender',
    ),
  );
  return $plugins;
}

/**
 * Implements hook_views_plugins_alter().
 */
function views_inject_view_results_views_plugins_alter(&$plugins) {
  $plugins['style']['default']['handler'] = 'ViewsInjectViewResultsViewsPluginStyle';
}

/**
 * Views Inject View Results form callback.
 *
 * @param $form
 *   An associative array containing the structure of the form.
 * @param $form_state
 *   A keyed array containing the current state of the form.
 *
 * @see: ViewsInjectViewResultsViewsPluginDisplayExtender::options_form()
 */
function views_inject_view_results_display_config_form_submit_temporary($form, &$form_state) {
  // With the line below if user reselects the display
  // the field will be populated as it was upon opening form.
  $form_state['values']['view_inject_order_field'] = NULL;
  $form_state['rerender'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Callback function used in options_form()
 *
 * @param array $element
 *   Form element.
 * @param $form_state
 *   An associative array containing the structure of the form.
 *
 * @return mixed
 *   Return form element.
 */
function _views_inject_view_results_option_descriptions(array $element, &$form_state) {
  foreach (element_children($element) as $key) {
    $element[$key]['#description'] = t('!description',
      array('!description' => $element['#options_descriptions'][$key])
    );
  }
  return $element;
}

README.txt

Views Inject View Results module allows to inject results from other view.
The results are ordered by ‘order field’ which should be in filtered content
type - for the view that is to be injected.
The injected results will remain in fixed position (value of the field) while
the primary view results will 'roll' around.
Please see the advanced help for more information.

Dependencies:
Views module: http://www.drupal.org/project/views

Installation:
Please refer to: http://www.drupal.org/node/120641
or for detailed information: http://www.drupal.org/node/895232

Known issues:
Upon uninstalling ‘Views Inject’ module you may get following error:   
  ( ! ) Fatal error: views_plugin_display::destroy()
  [<a href='views-plugin-display.destroy'>views-plugin-display.destroy</a>]:
  The script tried to execute a method or access a property of an incomplete
  object. Please ensure that the class definition &quot;
  ViewsInjectViewResultsViewsPluginDisplayExtender&quot;
  of the object you are trying to operate on was loaded _before_ unserialize()
  gets called or provide a __autoload() function to load the class definition
  in [site root path]/sites/all/modules/views/plugins/views_plugin_display.inc
  on line 272

This is the Views module issue, see: https://www.drupal.org/node/2040281
To correct you may:
- Clear Views cache with Drush:  
  $ drush cc views
- Or to permanently fix the issue apply
  views_plugin_display_extender_destroy_fix_D7-2040281-1.patch  
  see: https://www.drupal.org/node/2040281#comment-12133439
